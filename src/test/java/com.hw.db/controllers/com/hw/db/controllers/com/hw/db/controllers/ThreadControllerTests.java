import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.controllers.ThreadController;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;

import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ThreadControllerTests {

    private ThreadController controller;
    private User user;
    private Post post;
    private Thread thread;
    private Thread secondThread;

    @BeforeEach
    @DisplayName("Correct thread and user creation")
    void init() {
        controller = new ThreadController();

        thread = new Thread(
            "author", 
            new Timestamp(1), 
            "forum", 
            "msg", 
            "slug", 
            "title", 
            1
            );
        thread.setId(1);

        user = new User(
            "dmitriipolushin", 
            "d.polushin@innopolis.university", 
            "Dmitrii Polushin", 
            "about text"
            );
    }

    @Test
    @DisplayName("Correct post creation test")
    void createPostTest() {

        List<Post> posts = List.of(
            new Post(
            "dmitriipolushin", 
            new Timestamp(1), 
            "forum", "msg", 
            0, 
            0, 
            false
            )
        );


        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {

            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);

            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                userDAOMock.when(() -> UserDAO.Info("dmitriipolushin")).thenReturn(user);
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(posts),
                controller.createPost("slug", posts));
                assertEquals(thread, ThreadDAO.getThreadBySlug("slug"));
            }
        }

    }

    @Test
    @DisplayName("Correct post recieving test")
    void getPostsTest() {

        List<Post> posts = List.of(
            new Post(
            "dmitriipolushin", 
            new Timestamp(1), 
            "forum", "msg", 
            0, 
            0, 
            false
            )
        );

        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            threadDAOMock.when(() -> ThreadDAO.getPosts(1, 5, 0, "asc", true)).thenReturn(posts);

            ResponseEntity response = controller.Posts("slug", 5, 0, "asc", true);
            assertEquals(HttpStatus.OK, response.getStatusCode());
            assertEquals(List.of(post), response.getBody());
        }

    }

    @Test
    @DisplayName("Correct thread changing test")
    void changeThreadTest() {

        secondThread = new Thread(
            "author_2", 
            new Timestamp(1), 
            "forum_2", 
            "msg_2", 
            "slug_2", 
            "title_2", 
            2
            );
        
        secondThread.setId(2);


        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            threadDAOMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(secondThread);

            ResponseEntity response = controller.change("slug", thread);
            assertEquals(HttpStatus.OK, response.getStatusCode());
            assertEquals(secondThread, response.getBody());

        }
    }

    @Test
    @DisplayName("Correct thread retrieval test")
    void getThreadTest() {

        try (MockedStatic<ThreadDAO> threadDAOMocked = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMocked.when(() -> ThreadDAO.getThreadById(1)).thenReturn(thread);

            ResponseEntity response = controller.info(String.valueOf(1));
            assertEquals(HttpStatus.OK, response.getStatusCode());
            assertEquals(thread, response.getBody());
        }
    }

    @Test
    @DisplayName("Correct vote creation test")
    void createVoteTest() {
        final Vote vote = new Vote("dmitriipolushin", 1);
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                thread.setVotes(1);
                userDAOMock.when(() -> UserDAO.Info("dmitriipolushin")).thenReturn(user);
                threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
                threadDAOMock.when(() -> ThreadDAO.change(vote, thread.getVotes())).thenReturn(thread.getVotes());

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread),
                controller.createVote("slug", vote));
            }
        }

    }

}
